package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by phrc on 20/08/16.
 */

public class NewProject {

    public String name;
    public String description;
    public String companyId;
    public String startDate;
    public String endDate;
    @SerializedName("category-id")
    public String categoryId;

    public NewProject() {
    }

    public NewProject(String name,
                      String description,
                      String startDate,
                      String endDate,
                      String companyId,
                      String categoryId) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.companyId = companyId;
        this.categoryId = categoryId;
    }
}

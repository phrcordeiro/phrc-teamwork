package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by phrc on 20/08/16.
 */

public class CategoryResponse {

    @SerializedName("STATUS")
    public String status;
    public List<Category> categories;

}

package com.cordeiropedro.teamwork.model.entity;

import com.cordeiropedro.teamwork.model.connection.ApiClient;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by phrc on 20/08/16.
 */

public class ErrorResponse {

    @SerializedName("MESSAGE")
    public String message;
    @SerializedName("STATUS")
    public String status;

    public static ErrorResponse parseError(Response<?> response) {

        Converter<ResponseBody, ErrorResponse> converter = ApiClient.getClient()
                .responseBodyConverter(ErrorResponse.class, new Annotation[0]);

        try {
            return converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ErrorResponse();
        }
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Project {

    public boolean starred;
    public boolean replyByEmailEnabled;
    public boolean show_announcement;
    public boolean harvest_timers_enabled;
    public boolean filesAutoNewVersion;
    public boolean privacyEnabled;
    public boolean isProjectAdmin;
    public boolean notifyeveryone;
    public String status;
    public String subStatus;
    public String defaultPrivacy;
    public String logo;
    public String id;
    public String name;
    public String description;
    public String announcement;
    @SerializedName("start-page")
    public String startpage;
    public String announcementHTML;
    @SerializedName("created-on")
    public Date createdon;
    @SerializedName("last-changed-on")
    public Date lastchangedon;
    public Category category;
    public Company company;
    public Defaults defaults;
    public List<String> tags;

    public Project() {

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Project)) return false;

        Project project = (Project) o;

        return id.equals(project.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

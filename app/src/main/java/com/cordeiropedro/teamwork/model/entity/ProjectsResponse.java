package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by phrc on 19/08/16.
 */

public class ProjectsResponse {

    @SerializedName("STATUS")
    public String status;
    public List<Project> projects;

}

package com.cordeiropedro.teamwork.model.connection;

import com.cordeiropedro.teamwork.model.utility.Base64Coder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by phrc on 19/08/16.
 */

public class ApiClient {

    public static final String BASE_URL = "http://yat.teamwork.com/";
    public static final String API_KEY = "april294unreal";
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        if (retrofit == null) {

            OkHttpClient httpClient = new OkHttpClient().newBuilder().addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Chain chain) throws IOException {
                    Request request = chain
                            .request()
                            .newBuilder()
                            .addHeader("Authorization", encodeApiKey())
                            .build();
                    return chain.proceed(request);
                }
            }).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build();
        }
        return retrofit;
    }

    private static String encodeApiKey() {
        return "Basic " + Base64Coder.encodeString(API_KEY + ":" + "");
    }

}

package com.cordeiropedro.teamwork.model.connection.endpoint;

import com.cordeiropedro.teamwork.model.entity.CategoryResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by phrc on 20/08/16.
 */

public interface ICategoryRequester {

    @GET("projectCategories.json")
    Call<CategoryResponse> getCategoryList();

}

package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

public class Company {

    public String name;
    @SerializedName("is-owner")
    public String isowner;
    public String id;

    @Override
    public String toString() {
        return name;
    }
}

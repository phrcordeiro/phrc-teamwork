package com.cordeiropedro.teamwork.model.connection.endpoint;

import com.cordeiropedro.teamwork.model.entity.BasicResponse;
import com.cordeiropedro.teamwork.model.entity.NewProjectResponse;
import com.cordeiropedro.teamwork.model.entity.ProjectsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by phrc on 19/08/16.
 */

public interface IProjectRequester {

    @GET("projects.json")
    Call<ProjectsResponse> getProjectList();

    @PUT("projects/{project_id}/star.json")
    Call<BasicResponse> starProject(@Path("project_id") String projectId);

    @PUT("projects/{project_id}/unstar.json")
    Call<BasicResponse> unStarProject(@Path("project_id") String projectId);

    @POST("projects.json")
    Call<BasicResponse> createProject(@Body NewProjectResponse project);

}

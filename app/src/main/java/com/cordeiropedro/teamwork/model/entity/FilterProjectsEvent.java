package com.cordeiropedro.teamwork.model.entity;

import java.util.List;

/**
 * Created by phrc on 20/08/16.
 */

public class FilterProjectsEvent {

    public List<Project> projects;
    public String query;

    public FilterProjectsEvent(List<Project> projects, String query) {
        this.projects = projects;
        this.query = query;
    }
}

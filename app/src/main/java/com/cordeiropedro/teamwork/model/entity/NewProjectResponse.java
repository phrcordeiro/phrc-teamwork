package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by phrc on 20/08/16.
 */

public class NewProjectResponse {

    @SerializedName("project")
    public NewProject project;

    public NewProjectResponse() {
    }

    public NewProjectResponse(NewProject project) {
        this.project = project;
    }
}

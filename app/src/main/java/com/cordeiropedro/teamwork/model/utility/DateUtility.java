package com.cordeiropedro.teamwork.model.utility;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtility {

    public static final String MASK_TYPE_ONE = "yyyyMMdd";
    public static final String MASK_TYPE_TWO = "dd/MM/yyyy";

    public static String formatStringDate(String strDate, String formatIn, String formatOut) {
        try {
            DateFormat df = new SimpleDateFormat(formatIn);
            DateFormat df2 = new SimpleDateFormat(formatOut);
            return df2.format(df.parse(strDate));
        } catch (Exception e) {
            return null;
        }
    }

    public static String dateForString(Date date, String format) {
        if (date != null) {
            DateFormat df = new SimpleDateFormat(format);
            return df.format(date);
        }
        return "";
    }

}

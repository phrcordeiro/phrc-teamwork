package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by phrc on 20/08/16.
 */

public class CompanyResponse {

    @SerializedName("STATUS")
    public String status;
    public List<Company> companies;

}

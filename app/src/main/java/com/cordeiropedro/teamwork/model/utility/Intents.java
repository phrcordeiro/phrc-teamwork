package com.cordeiropedro.teamwork.model.utility;

import android.content.Context;
import android.content.Intent;

import com.cordeiropedro.teamwork.view.activity.AddProjectActivity;

/**
 * Created by phrc on 20/08/16.
 */

public class Intents {

    public static Intent goToAddProject(Context context) {
        return new Intent(context, AddProjectActivity.class);
    }
}

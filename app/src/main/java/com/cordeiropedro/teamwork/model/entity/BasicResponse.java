package com.cordeiropedro.teamwork.model.entity;

import com.google.gson.annotations.SerializedName;

/**
 * Created by phrc on 19/08/16.
 */

public class BasicResponse {

    @SerializedName("MESSAGE")
    public String message;

}

package com.cordeiropedro.teamwork.presenter;

import com.cordeiropedro.teamwork.model.connection.ApiClient;
import com.cordeiropedro.teamwork.model.connection.endpoint.IProjectRequester;
import com.cordeiropedro.teamwork.model.entity.BasicResponse;
import com.cordeiropedro.teamwork.model.entity.EmptyEvent;
import com.cordeiropedro.teamwork.model.entity.ErrorResponse;
import com.cordeiropedro.teamwork.model.entity.FilterProjectsEvent;
import com.cordeiropedro.teamwork.model.entity.Project;
import com.cordeiropedro.teamwork.model.entity.ProjectsResponse;
import com.cordeiropedro.teamwork.view.activity.IMainActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by phrc on 19/08/16.
 */

public class MainPresenter {

    IMainActivity mainView;

    public MainPresenter(IMainActivity mainView) {
        this.mainView = mainView;
        EventBus.getDefault().register(this);
    }


    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onProjectListEvent(EmptyEvent emptyEvent) {
        IProjectRequester projectRequester = ApiClient.getClient().create(IProjectRequester.class);
        Call<ProjectsResponse> call = projectRequester.getProjectList();
        call.enqueue(new Callback<ProjectsResponse>() {
            @Override
            public void onResponse(Call<ProjectsResponse> call, Response<ProjectsResponse> response) {
                if (!response.isSuccessful()) {
                    ErrorResponse error = ErrorResponse.parseError(response);
                    if (error.message != null) {
                        mainView.notifyRequestError(new Throwable(error.message));
                    }

                } else {
                    mainView.updateProjectList(response.body().projects);
                }
                mainView.progressOFF();
            }

            @Override
            public void onFailure(Call<ProjectsResponse> call, Throwable t) {
                mainView.notifyRequestError(t);
                mainView.progressOFF();
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onStarredEvent(final Project project) {
        IProjectRequester projectRequester = ApiClient.getClient().create(IProjectRequester.class);
        Call<BasicResponse> call = (project.starred) ?
                projectRequester.starProject(project.id) :
                projectRequester.unStarProject(project.id);
        call.enqueue(new Callback<BasicResponse>() {
            @Override
            public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                if (!response.isSuccessful()) {
                    project.starred = !project.starred;
                    mainView.updateProjectList();
                    ErrorResponse error = ErrorResponse.parseError(response);
                    if (error.message != null) {
                        mainView.notifyRequestError(new Throwable(error.message));
                    }
                }
            }

            @Override
            public void onFailure(Call<BasicResponse> call, Throwable t) {
                project.starred = !project.starred;
                mainView.updateProjectList();
                mainView.notifyRequestError(t);
            }
        });

        project.starred = !project.starred;
        mainView.updateProjectList();
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onStarredEvent(FilterProjectsEvent filterProjectsEvent) {
        mainView.filterProjectList(filter(filterProjectsEvent));
    }

    private List<Project> filter(FilterProjectsEvent filter) {
        filter.query = filter.query.toLowerCase();

        final List<Project> filteredModelList = new ArrayList<>();
        for (Project model : filter.projects) {
            final String text = model.name.toLowerCase();
            if (text.contains(filter.query)) {
                filteredModelList.add(model);
            }
        }
        return filteredModelList;
    }

}

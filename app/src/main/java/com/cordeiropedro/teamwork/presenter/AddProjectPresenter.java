package com.cordeiropedro.teamwork.presenter;

import com.cordeiropedro.teamwork.model.connection.ApiClient;
import com.cordeiropedro.teamwork.model.connection.endpoint.ICategoryRequester;
import com.cordeiropedro.teamwork.model.connection.endpoint.ICompanyRequester;
import com.cordeiropedro.teamwork.model.connection.endpoint.IProjectRequester;
import com.cordeiropedro.teamwork.model.entity.BasicResponse;
import com.cordeiropedro.teamwork.model.entity.Category;
import com.cordeiropedro.teamwork.model.entity.CategoryResponse;
import com.cordeiropedro.teamwork.model.entity.Company;
import com.cordeiropedro.teamwork.model.entity.CompanyResponse;
import com.cordeiropedro.teamwork.model.entity.ErrorResponse;
import com.cordeiropedro.teamwork.model.entity.NewProjectResponse;
import com.cordeiropedro.teamwork.view.activity.IAddProjectActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by phrc on 20/08/16.
 */

public class AddProjectPresenter {

    IAddProjectActivity view;

    static boolean isMenuPressed = false;

    public AddProjectPresenter(IAddProjectActivity view) {
        this.view = view;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onCompanyListEvent(Company company) {
        ICompanyRequester companyRequester = ApiClient.getClient().create(ICompanyRequester.class);
        Call<CompanyResponse> call = companyRequester.getCompanyList();
        call.enqueue(new Callback<CompanyResponse>() {
            @Override
            public void onResponse(Call<CompanyResponse> call, Response<CompanyResponse> response) {
                if (!response.isSuccessful()) {
                    ErrorResponse error = ErrorResponse.parseError(response);
                    if (error.message != null) {
                        view.notifyRequestError(new Throwable(error.message));
                    }
                } else {
                    view.getCompanies(response.body().companies);
                }
            }

            @Override
            public void onFailure(Call<CompanyResponse> call, Throwable t) {
                view.notifyRequestError(t);
            }
        });

    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onCategoryListEvent(Category category) {
        ICategoryRequester categoryRequester = ApiClient.getClient().create(ICategoryRequester.class);
        Call<CategoryResponse> call = categoryRequester.getCategoryList();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (!response.isSuccessful()) {
                    ErrorResponse error = ErrorResponse.parseError(response);
                    if (error.message != null) {
                        view.notifyRequestError(new Throwable(error.message));
                    }

                } else {
                    view.getCategory(response.body().categories);
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                view.notifyRequestError(t);
            }
        });
    }

    @Subscribe(threadMode = ThreadMode.POSTING)
    public void onCreateProjectEvent(NewProjectResponse project) {
        if (!isMenuPressed) {
            isMenuPressed = true;
            view.progressON();
            IProjectRequester projectRequester = ApiClient.getClient().create(IProjectRequester.class);
            Call<BasicResponse> call = projectRequester.createProject(project);
            call.enqueue(new Callback<BasicResponse>() {
                @Override
                public void onResponse(Call<BasicResponse> call, Response<BasicResponse> response) {
                    if (!response.isSuccessful()) {
                        ErrorResponse error = ErrorResponse.parseError(response);
                        if (error.message != null) {
                            view.notifyRequestError(new Throwable(error.message));
                        }
                    } else {
                        view.onCreatedProject();
                    }
                    isMenuPressed = false;
                    view.progressOFF();
                }


                @Override
                public void onFailure(Call<BasicResponse> call, Throwable t) {
                    view.notifyRequestError(t);
                    isMenuPressed = false;
                    view.progressOFF();
                }
            });
        }
    }

}

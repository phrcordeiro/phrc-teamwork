package com.cordeiropedro.teamwork.view.activity;

import com.cordeiropedro.teamwork.model.entity.Project;

import java.util.List;

/**
 * Created by phrc on 19/08/16.
 */

public interface IMainActivity {

    void updateProjectList(List<Project> projects);

    void updateProjectList();

    void filterProjectList(List<Project> projects);

    void notifyRequestError(Throwable throwable);

    void progressON();

    void progressOFF();
}

package com.cordeiropedro.teamwork.view.holder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.model.entity.Project;
import com.cordeiropedro.teamwork.model.utility.DateUtility;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class ProjectHolder extends AbstractRecyclerViewHolder {

    @BindView(R.id.project_name)
    public TextView projectName;
    @BindView(R.id.project_create_date)
    public TextView projectCreateDate;
    @BindView(R.id.company_name)
    public TextView companyName;
    @BindView(R.id.project_logo)
    public ImageView projectLogo;
    @BindView(R.id.stared)
    public ImageView stared;

    public ProjectHolder(View itemView) {
        super(itemView);
    }

    public void bind(final Project project) {
        projectName.setText(project.name);
        companyName.setText(project.company.name);
        projectCreateDate.setText(
                DateUtility.dateForString(project.createdon, DateUtility.MASK_TYPE_TWO));
        ImageLoader.getInstance().displayImage(project.logo, projectLogo);
        stared.setImageResource(
                (project.starred) ? R.drawable.ic_full_star : R.drawable.ic_empty_star);
        stared.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post(project);
            }
        });
    }
}

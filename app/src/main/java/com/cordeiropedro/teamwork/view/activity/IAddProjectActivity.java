package com.cordeiropedro.teamwork.view.activity;

import com.cordeiropedro.teamwork.model.entity.Category;
import com.cordeiropedro.teamwork.model.entity.Company;

import java.util.List;

/**
 * Created by phrc on 20/08/16.
 */

public interface IAddProjectActivity {

    void getCompanies(List<Company> companies);

    void getCategory(List<Category> categories);

    void notifyRequestError(Throwable throwable);

    void onCreatedProject();

    void progressON();

    void progressOFF();

}

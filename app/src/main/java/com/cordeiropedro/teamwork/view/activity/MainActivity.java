package com.cordeiropedro.teamwork.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.model.entity.EmptyEvent;
import com.cordeiropedro.teamwork.model.entity.FilterProjectsEvent;
import com.cordeiropedro.teamwork.model.entity.Project;
import com.cordeiropedro.teamwork.model.utility.Intents;
import com.cordeiropedro.teamwork.presenter.MainPresenter;
import com.cordeiropedro.teamwork.view.adpter.ProjectAdapter;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by phrc on 19/08/16.
 */

public class MainActivity extends AppCompatActivity implements IMainActivity {

    @BindView(R.id.project_list)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.swiperefresh)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.relative_layout)
    RelativeLayout relativeLayout;
    @BindView(R.id.add_project)
    FloatingActionButton addProject;

    ProjectAdapter adapter;
    RecyclerView.LayoutManager mLayoutManager;
    List<Project> projectModels;

    MainPresenter presenter;

    //region LifeCycle and Menu Methods

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        presenter = new MainPresenter(this);
        setSwipeRefresh();
        projectListSetup();
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressON();
        EventBus.getDefault().post(new EmptyEvent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.project_search_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                EventBus.getDefault().post(new FilterProjectsEvent(projectModels, newText));
                return true;
            }

        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                adapter.setFilter(projectModels);
                return true;
            }
        });

        return true;

    }

    //endregion

    //region OnClick

    @OnClick(R.id.add_project)
    void setAddProjectClick() {
        startActivity(Intents.goToAddProject(this));
    }

    //endregion

    //region Private Methods

    private void setSwipeRefresh() {
        swipeRefreshLayout.setColorSchemeResources(R.color.blue);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                EventBus.getDefault().post(new EmptyEvent());
            }
        });
    }

    private void projectListSetup() {
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        projectModels = new ArrayList<>();
        adapter = new ProjectAdapter(projectModels, this);
        mRecyclerView.setAdapter(adapter);
    }

    //endregion

    //region IMainActivity Implementation

    @Override
    public void updateProjectList(List<Project> projects) {
        projectModels.clear();
        projectModels.addAll(projects);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateProjectList() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void filterProjectList(List<Project> projects) {
        adapter.setFilter(projects);
    }

    @Override
    public void notifyRequestError(Throwable throwable) {
        Snackbar.make(relativeLayout, throwable.getMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void progressON() {
        progressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
    }

    @Override
    public void progressOFF() {
        progressBar.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    //endregion
}

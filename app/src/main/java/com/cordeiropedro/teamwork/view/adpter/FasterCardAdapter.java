package com.cordeiropedro.teamwork.view.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public abstract class FasterCardAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    protected List<T> listObjects;
    protected View view;
    Context context;

    protected abstract int layoutResourceForItem(int position);

    protected abstract void fillHolder(RecyclerView.ViewHolder abstractHolder, int position);

    protected abstract RecyclerView.ViewHolder inflateView(View view, int position);

    public FasterCardAdapter(List<T> listObjects, Context context) {
        this.listObjects = listObjects;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return inflateView(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(layoutResourceForItem(viewType), viewGroup, false),
                viewType);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder abstractHolder, int i) {
        fillHolder(abstractHolder, i);
    }

    @Override
    public int getItemCount() {
        return listObjects.size();
    }

    protected Context getContext() {
        return context;
    }

    protected T getItem(int position) {
        return listObjects.get(position);
    }
}

package com.cordeiropedro.teamwork.view.fragment;

import android.widget.DatePicker;

public interface IDatePickerListern {

    public void onDateSet(DatePicker datePicker, int i, int i1, int i2);

}

package com.cordeiropedro.teamwork.view.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.model.entity.Project;
import com.cordeiropedro.teamwork.view.holder.ProjectHolder;

import java.util.ArrayList;
import java.util.List;

public class ProjectAdapter extends FasterCardAdapter<Project> {

    public ProjectAdapter(List<Project> listObjects, Context context) {
        super(listObjects, context);
    }

    @Override
    protected int layoutResourceForItem(int position) {
        return R.layout.card_project;
    }

    @Override
    protected void fillHolder(RecyclerView.ViewHolder abstractHolder, int position) {
        ProjectHolder holder = (ProjectHolder) abstractHolder;
        Project model = listObjects.get(position);
        holder.bind(model);
    }

    @Override
    protected RecyclerView.ViewHolder inflateView(View view, int position) {
        return new ProjectHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder abstractHolder, int i) {
        super.onBindViewHolder(abstractHolder, i);
        Project model = listObjects.get(i);
        ((ProjectHolder) abstractHolder).bind(model);
    }

    public void setFilter(List<Project> projects) {
        listObjects = new ArrayList<>();
        listObjects.addAll(projects);
        notifyDataSetChanged();
    }
}

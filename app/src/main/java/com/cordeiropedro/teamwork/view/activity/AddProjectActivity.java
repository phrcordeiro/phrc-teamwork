package com.cordeiropedro.teamwork.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.cordeiropedro.teamwork.R;
import com.cordeiropedro.teamwork.model.entity.Category;
import com.cordeiropedro.teamwork.model.entity.Company;
import com.cordeiropedro.teamwork.model.entity.NewProject;
import com.cordeiropedro.teamwork.model.entity.NewProjectResponse;
import com.cordeiropedro.teamwork.model.utility.DateUtility;
import com.cordeiropedro.teamwork.presenter.AddProjectPresenter;
import com.cordeiropedro.teamwork.view.fragment.DatePickerFragment;
import com.cordeiropedro.teamwork.view.fragment.IDatePickerListern;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTouch;

/**
 * Created by phrc on 20/08/16.
 */

public class AddProjectActivity extends AppCompatActivity implements IAddProjectActivity {

    @BindView(R.id.layout)
    LinearLayout layout;
    @BindView(R.id.layout_form)
    LinearLayout layout_form;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.list_category)
    AppCompatSpinner categorySpinner;
    @BindView(R.id.list_company)
    AppCompatSpinner companySpinner;
    @BindView(R.id.end_date)
    EditText endDate;
    @BindView(R.id.start_date)
    EditText startDate;
    @BindView(R.id.input_name)
    EditText name;
    @BindView(R.id.input_description)
    EditText description;

    List<Company> companyList = new ArrayList<Company>();
    ArrayAdapter<Company> companyArrayAdapter;

    List<Category> categoryList = new ArrayList<Category>();
    ArrayAdapter<Category> categoryArrayAdapter;

    AddProjectPresenter presenter;

    boolean isViewTouch = false;

    //region LifeCyle and Menu

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_project);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new AddProjectPresenter(this);
        setupCompanySpinner();
        setupCategorySpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.save:
                EventBus.getDefault().post(new NewProjectResponse(new NewProject(
                        name.getText().toString(),
                        description.getText().toString(),
                        DateUtility.formatStringDate(startDate.getText().toString(),
                                DateUtility.MASK_TYPE_TWO,
                                DateUtility.MASK_TYPE_ONE),
                        DateUtility.formatStringDate(endDate.getText().toString(),
                                DateUtility.MASK_TYPE_TWO,
                                DateUtility.MASK_TYPE_ONE),
                        ((Company) companySpinner.getSelectedItem()).id,
                        ((Category) categorySpinner.getSelectedItem()).id

                )));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save_menu, menu);
        return true;
    }

    //endregion

    //region OnClick Methods

    @OnTouch(R.id.start_date)
    public boolean setStartDateClick() {
        if (!isViewTouch) {
            isViewTouch = true;
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setDateSetListern(new IDatePickerListern() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    String month = String.valueOf((i1 < 10) ? "0" + (i1 + 1) : (i1 + 1));
                    String day = String.valueOf((i2 < 10) ? "0" + i2 : i2);
                    startDate.setText(day + "/" + month + "/" + i);
                    isViewTouch = false;
                }
            });
            newFragment.show(getSupportFragmentManager(), "date");
        }
        return true;
    }

    @OnTouch(R.id.end_date)
    public boolean setEndDateClick() {
        if (!isViewTouch) {
            isViewTouch = true;
            DatePickerFragment newFragment = new DatePickerFragment();
            newFragment.setDateSetListern(new IDatePickerListern() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    String month = String.valueOf((i1 < 10) ? "0" + (i1 + 1) : (i1 + 1));
                    String day = String.valueOf((i2 < 10) ? "0" + i2 : i2);
                    endDate.setText(day + "/" + month + "/" + i);
                    isViewTouch = false;
                }
            });
            newFragment.show(getSupportFragmentManager(), "date");
        }
        return true;
    }

    //endregion

    //region Private Methods

    private void setupCompanySpinner() {
        companyArrayAdapter =
                new ArrayAdapter<Company>(getApplicationContext(),
                        R.layout.spinner_style,
                        companyList);
        companyArrayAdapter.setDropDownViewResource(R.layout.spinner_style);
        companySpinner.setAdapter(companyArrayAdapter);
        EventBus.getDefault().post(new Company());
    }

    private void setupCategorySpinner() {
        categoryArrayAdapter =
                new ArrayAdapter<Category>(getApplicationContext(),
                        R.layout.spinner_style,
                        categoryList);
        categoryArrayAdapter.setDropDownViewResource(R.layout.spinner_style);
        categorySpinner.setAdapter(categoryArrayAdapter);
        EventBus.getDefault().post(new Category());
    }


    //endregion

    //region IAddProjectActivity Implementation

    @Override
    public void getCompanies(List<Company> companies) {
        companyList.clear();
        companyList.addAll(companies);
        companyArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void getCategory(List<Category> categories) {
        categoryList.clear();
        categoryList.addAll(categories);
        categoryArrayAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyRequestError(Throwable throwable) {
        Snackbar.make(layout, throwable.getMessage(), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onCreatedProject() {
        finish();
    }

    @Override
    public void progressON() {
        progressBar.setVisibility(View.VISIBLE);
        layout_form.setVisibility(View.GONE);
    }

    @Override
    public void progressOFF() {
        progressBar.setVisibility(View.GONE);
        layout_form.setVisibility(View.VISIBLE);
    }


    //endregion


}
